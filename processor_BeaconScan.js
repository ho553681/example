var config = require('../config/config_BeaconScanner.json');
var log = require('../tools/logger_console');
let logger = {
	error: function err(text) {
		log.error(text, { curFileName: __filename, curFunctionName: err.caller.name });
	},
	warn: function warn(text) {
		log.warn(text, { curFileName: __filename, curFunctionName: warn.caller.name });
	},
	info: function info(text) {
		log.info(text, { curFileName: __filename, curFunctionName: info.caller.name });
	},
	debug: function debug(text) {
		log.debug(text, { curFileName: __filename, curFunctionName: debug.caller.name });
	}
};

logger.info(`GC ${global.gc? "" : "not "}available`);

const MqttClient = require('../mqtt/client');
const BeaconScanner = require('node-beacon-scanner');
const scanner = new BeaconScanner();
var db = require('../db/sqlite_TrackBeacon');

// setup

const Interval_requestBeaconList = 900; // 15 mins if no update from ColdBoxProcessor
const Interval_reportBeacon = 5;

var client_mqtt = new MqttClient(config.mqtt_info, {
	onMessage: onMqttMessage
});

// operation

scanner.onadvertisement = processBeaconAd;

async function init() {
	try {
		await db.init(10);
	//	await test_list();
		requestBeaconList();
		scanner.startScan();
		console.log("Beacon scanner started.");
	} catch (ex) {
		logger.error(ex);
	}
}

function requestBeaconList() {
	client_mqtt.publishMessage("ATAL/Beacon/Filter/List/Req", {});
}

async function reportBeacon() {
	client_mqtt.publishMessage("ATAL/Beacon/Status", await db.getBeaconReception());
}

init();
setInterval(requestBeaconList, Interval_requestBeaconList * 1000);
setInterval(checkBeaconStatus, Interval_reportBeacon * 1000);

// handlers

async function processBeaconAd(ad) {
	try {
		let addr = ad.address.replace(/:/g, '');
		let batt;
		if (typeof ad.eddystoneTlm === 'object') batt = ad.eddystoneTlm.batteryVoltage;
		let data = await db.updateAvgBeaconReception(addr, ad.rssi, batt);
	//	console.log(data);
		data.forEach(function (row) {
			logger.info(`LocationID: ${row.LocationID} RSSI: ${row.RSSI_avg.toFixed(2)} from ${row.Count} data points. ${typeof row.BatteryVoltage === "number"? `Batt: ${row.BatteryVoltage/1000}V` : ""}`);
		});
	//	logger.info(`Most likely at Zone ${data[0].LocationID}`);
		logger.info("----------");
	//	await db.printBeaconReception();
	} catch (ex) {
		if (!ex.startsWith("Beacon not found:")) logger.error(ex);
	} finally {
		if (global) {
			if (global.gc) global.gc();
		}
	}
	return;
}

async function checkBeaconStatus() {
	try {
	//	logger.info("checking beacon status...");
		let count = await db.checkBeaconList();
	//	await test_list();
		if (count == 0) {
			logger.info("no beacon in list!");
			requestBeaconList();
		} else {
		//	logger.info("reporting beacon reception");
			await reportBeacon();
		}
	} catch (err) {
		logger.error(ex);
	}
}

async function onMqttMessage(topic, message) {
	let msgObject;
	try {
		let targets = topic.split('/');
		// abort if format does not matchk,
		if (targets[0] !== "ATAL")		return;
		if (targets[1] !== "Beacon")	return;
		switch (targets[2]) {
		case "Filter":
			msgObject = JSON.parse(message);
			switch (targets[3]) {
			case "List":
				if (targets[4] !== "Resp")	return;
			//	await db.clearBeaconList();
				await db.updateBeaconList(msgObject);
			//	await db.printBeaconList();
				logger.info(`Updated Beacon List`);
				break;
			case "Update":
				if (targets[4] !== "Req")	return;
				if (typeof msgObject.id !== 'number') throw "invalid LocationID";
				if (typeof msgObject.mac !== 'string') throw "invalid MAC";
				await db.updateBeaconAddr(msgObject.id, msgObject.mac);
				client_mqtt.publishMessage("ATAL/Beacon/List/Update/Resp", msgObject);
				logger.info(`Updated Beacon ${msgObject.id}: ${msgObject.mac}`);
				break;
			case "Delete":
				if (targets[4] !== "Req")	return;
				if (typeof msgObject.id !== 'number') throw "invalid LocationID";
				if (typeof msgObject.mac !== 'string') throw "invalid MAC";
				await db.deleteBeacon(msgObject.id, msgObject.mac);
				client_mqtt.publishMessage("ATAL/Beacon/List/Delete/Resp", msgObject);
				logger.info(`Deleted Beacon ${msgObject.id}: ${msgObject.mac}`);
				break;
			}
			break;
		case "Print":
			switch (targets[3]) {
			case "Count":
				let count = await db.checkBeaconList();
				logger.info(`Location Beacon Count: ${count}`);
				client_mqtt.publishMessage("ATAL/Beacon/Debug/Count", {Count: count});
				break;
			case "List":
				let list = await db.printBeaconList();
				client_mqtt.publishMessage("ATAL/Beacon/Debug/List", list);
				break;
			}
			break;
		}
	} catch (e1) {
		logger.error(e1);
	}
}

// test

async function test_list() {
	let BeaconList = [
		{id: 101, mac: "fc9e29d8a62f"},
		{id: 102, mac: "d6080af7e277"},
		{id: 103, mac: "de46300ceb35"},
		{id: 104, mac: "c13ce0dc9a6d"},
	];
//	console.log("updateBeaconList");
	return db.updateBeaconList(BeaconList);
}
/*
function calculateDistance(txPower, rssi) {
	if (txPower == null) {
		throw "invalid txPower";
	}
	if (rssi === 0) {
		throw "cannot determine accuracy"; // if we cannot determine accuracy, return -1.
	}

	var ratio = rssi * 1 / txPower;
	return Math.pow(ratio, 10);
	if (ratio < 1.0) {
		return Math.pow(ratio, 10);
	} else {
		return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
	}
}
const iBeaconList = {
//	"fc9e29d8a62f": {mapping: 0, distance: 5.44},
	"d6080af7e277": {mapping: 1, distance: 4.30},
//	"de46300ceb35": {mapping: 2, distance: 2.06},
	"c13ce0dc9a6d": {mapping: 3, distance: 0},
};
*/